# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Aputsiak Niels Janussen (Skjalden) <aj@isit.gl>, 2013
# Aputsiak Niels Janussen (Skjalden) <aj@isit.gl>, 2013
# Linuxbruger <y.z@live.dk>, 2018
# Per Kongstad <p_kongstad@op.pl>, 2009,2016
# scootergrisen, 2017
# scootergrisen, 2017-2019
msgid ""
msgstr ""
"Project-Id-Version: Garcon\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-06-20 18:30+0200\n"
"PO-Revision-Date: 2019-06-17 18:58+0000\n"
"Last-Translator: scootergrisen\n"
"Language-Team: Danish (http://www.transifex.com/xfce/garcon/language/da/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: da\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/xfce/xfce-accessories.directory.in.h:1
msgid "Accessories"
msgstr "Tilbehør"

#: ../data/xfce/xfce-accessories.directory.in.h:2
msgid "Common desktop tools and applications"
msgstr "Almene skrivebordsværktøjer og -programmer"

#: ../data/xfce/xfce-development.directory.in.h:1
msgid "Development"
msgstr "Udvikling"

#: ../data/xfce/xfce-development.directory.in.h:2
msgid "Software development tools"
msgstr "Værktøjer til softwareudvikling"

#: ../data/xfce/xfce-education.directory.in.h:1
msgid "Education"
msgstr "Læring"

#: ../data/xfce/xfce-education.directory.in.h:2
msgid "Educational software"
msgstr "Programmer til læring"

#: ../data/xfce/xfce-games.directory.in.h:1
msgid "Games"
msgstr "Spil"

#: ../data/xfce/xfce-games.directory.in.h:2
msgid "Games, puzzles, and other fun software"
msgstr "Spil, hjernegymnastik og andet sjov software"

#: ../data/xfce/xfce-graphics.directory.in.h:1
msgid "Graphics"
msgstr "Grafik"

#: ../data/xfce/xfce-graphics.directory.in.h:2
msgid "Graphics creation and manipulation applications"
msgstr "Programmer til oprettelse og manipulering af grafik"

#: ../data/xfce/xfce-hardware.directory.in.h:1
msgid "Hardware"
msgstr "Hardware"

#: ../data/xfce/xfce-hardware.directory.in.h:2
msgid "Settings for several hardware devices"
msgstr "Indstillinger for adskillige hardwareenheder"

#: ../data/xfce/xfce-multimedia.directory.in.h:1
msgid "Multimedia"
msgstr "Multimedie"

#: ../data/xfce/xfce-multimedia.directory.in.h:2
msgid "Audio and video players and editors"
msgstr "Lyd- og videoafspillere samt editorer"

#: ../data/xfce/xfce-network.directory.in.h:1
msgid "Internet"
msgstr "Internet"

#: ../data/xfce/xfce-network.directory.in.h:2
msgid "Applications for Internet access"
msgstr "Programmer til internetadgang"

#: ../data/xfce/xfce-office.directory.in.h:1
msgid "Office"
msgstr "Kontor"

#: ../data/xfce/xfce-office.directory.in.h:2
msgid "Office and productivity applications"
msgstr "Kontor- og produktivitetsprogrammer"

#: ../data/xfce/xfce-other.directory.in.h:1
msgid "Other"
msgstr "Andre"

#: ../data/xfce/xfce-other.directory.in.h:2
msgid "Applications that don't fit into other categories"
msgstr "Programmer som ikke passer i de andre kategorier"

#: ../data/xfce/xfce-personal.directory.in.h:1
msgid "Personal"
msgstr "Personlige"

#: ../data/xfce/xfce-personal.directory.in.h:2
msgid "Personal settings"
msgstr "Personlige indstillinger"

#: ../data/xfce/xfce-screensavers.directory.in.h:1
msgid "Screensavers"
msgstr "Pauseskærme"

#: ../data/xfce/xfce-screensavers.directory.in.h:2
msgid "Screensaver applets"
msgstr "Pauseskærm-appletter"

#: ../data/xfce/xfce-settings.directory.in.h:1
msgid "Settings"
msgstr "Indstillinger"

#: ../data/xfce/xfce-settings.directory.in.h:2
msgid "Desktop and system settings applications"
msgstr "Programmer til skrivebords- og systemindstillinger"

#: ../data/xfce/xfce-system.directory.in.h:1
msgid "System"
msgstr "System"

#: ../data/xfce/xfce-system.directory.in.h:2
msgid "System tools and utilities"
msgstr "Systemværktøjer og -redskaber"

#: ../garcon/garcon-menu.c:702
#, c-format
msgid "File \"%s\" not found"
msgstr "Filen \"%s\" blev ikke fundet"

#: ../garcon/garcon-menu-parser.c:276
#, c-format
msgid "Could not load menu file data from %s: %s"
msgstr "Kunne ikke indlæse data for menufil fra %s: %s"

#: ../garcon/garcon-menu-parser.c:283
#, c-format
msgid "Could not load menu file data from %s"
msgstr "Kunne ikke indlæse data for menufil fra %s"

#: ../garcon-gtk/garcon-gtk-menu.c:446
#, c-format
msgid "Failed to execute command \"%s\"."
msgstr "Kunne ikke køre kommandoen \"%s\"."

#: ../garcon-gtk/garcon-gtk-menu.c:472
msgid "Launch Error"
msgstr "Fejl ved start"

#: ../garcon-gtk/garcon-gtk-menu.c:474
msgid ""
"Unable to launch \"exo-desktop-item-edit\", which is required to create and "
"edit menu items."
msgstr "Kunne ikke starte \"exo-desktop-item-edit\", som er nødvendigt for at oprette eller redigere menupunkter."

#: ../garcon-gtk/garcon-gtk-menu.c:476
msgid "_Close"
msgstr "_Luk"

#: ../garcon-gtk/garcon-gtk-menu.c:988
msgid "Failed to load the applications menu"
msgstr "Kunne ikke indlæse programmenuen"
